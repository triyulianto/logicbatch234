﻿using System;

namespace tugas3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tugas Logic 3");
            Console.WriteLine("------------------");
            Console.WriteLine();

            //Soal 1
            Console.WriteLine("Soal 1\n");
            Console.Write("Kode\t: ");
            string kode = Console.ReadLine();
            int jumlah = 0;
            for (int i = 0; i < kode.Length; i += 3)
            {
                string match = kode.Substring(i, 3);    /*Mengambil tiga angka dimulai index i*/
                if (match.ToUpper() != "SOS")
                {
                    jumlah++;
                }
            }
            Console.WriteLine("jumlah kode salah\t: " + jumlah);
            Console.WriteLine("\n");


            //Soal 2
            Console.WriteLine("Soal 2\n");
            Console.Write("deret\t\t: ");
            string deret = Console.ReadLine();
            Console.WriteLine();
            string[] myArray = deret.Split(",");
            string temp0 = "";
            string temp1 = "";
            Console.WriteLine("Deret angka\t: " + string.Join(",", myArray));
            Console.Write("Masukkan jumlah rotasi\t: ");
            int input = int.Parse(Console.ReadLine());
            int no = 1;
            Console.WriteLine();
            for (int x = 0; x < input; x++)
            {
                for (int i = myArray.Length - 1; i >= 0; i--)
                {
                    if (i == myArray.Length - 1)
                    {
                        temp0 = myArray[i];
                        myArray[i] = myArray[0];
                    }
                    else
                    {
                        temp1 = myArray[i];
                        myArray[i] = temp0;
                        temp0 = temp1;
                    }
                }
                Console.WriteLine("\tHasil Rotasi Ke-" + no + ": " + string.Join(",", myArray));
                no++;
            }
            Console.WriteLine();
            string output = string.Join(",", myArray);
            Console.WriteLine("Deret akhir : " + output);

            Console.WriteLine();

            //Soal 3
            Console.WriteLine("Soal 3\n");
            Console.Write("Jumlah putung\t: ");
            int putung = int.Parse(Console.ReadLine());
            putung /= 8;
            int keuntungan = putung * 500;
            Console.WriteLine("Jumlah Rokok\t: " + putung);
            Console.WriteLine("Keuntungan\t: Rp." + keuntungan);
            Console.WriteLine("\n");

            //Soal 4
            Console.WriteLine("Soal 4\n");
            Console.Write("Uang Andi\t: ");
            int uang = int.Parse(Console.ReadLine());
            Console.WriteLine();
            int[,] harga = { { 35, 40, 50, 20 }, { 40, 30, 45, 10 } };
            int[] setelan = new int[4];
            int temp = 0;
            int a = 0;
            int b = 0;
            int beli = 0;
            no = 1;

            if (uang < 30)
            {
                Console.WriteLine("Daftar Barang");
                Console.WriteLine("--------------");
                for (int i = 0; i < 4; i++)
                {
                    setelan[i] = harga[0, i] + harga[1, i];
                    Console.WriteLine("\t" + no + ". " + setelan[i]);
                    no++;
                }
                Console.WriteLine();
                Console.WriteLine("Uang Andi tidak cukup");
                Console.WriteLine("Uang Andi\t: " + uang);
            }
            else
            {
                Console.WriteLine("Daftar Barang");
                Console.WriteLine("--------------");
                for (int i = 0; i < 4; i++)
                {
                    setelan[i] = harga[0, i] + harga[1, i];
                    Console.WriteLine("\t" + no + ". " + setelan[i]);
                    no++;
                    a = uang - setelan[i];
                    b = uang - temp;
                    if (a < b && uang >= setelan[i])
                    {
                        beli = setelan[i];
                        temp = setelan[i];
                    }
                    else if (b < a)
                    {
                        beli = temp;
                    }
                }
                Console.WriteLine("Barang yang dibeli Andi\t: " + beli);
                Console.WriteLine("Uang Andi\t\t: " + uang);
            }
            Console.WriteLine("\n");

            //Soal 5
            Console.WriteLine("Soal 5\n");
            string tmp = "";
            Console.Write("Masukkan kata\t: ");
            string kata = Console.ReadLine();
            string[] split = kata.Split(' ');
            
            for(int j=0; j<split.Length;j++)
            {
                string tempkata = split[j];
                for(int i = tempkata.Length-1;i>=0;i--)
                {
                    tmp += tempkata[i].ToString();
                }
                Console.Write("\tKata ");
                if (tmp.ToUpper()==split[j].ToUpper())
                {
                    Console.WriteLine("'"+tempkata+ "'\t: Palindrome");
                    tmp = "";
                }
                else
                {
                    Console.WriteLine("'"+tempkata+ "'\t: Bukan Palindrome");
                    tmp = "";
                }
            }

            Console.WriteLine();
            
            //-----------------------------------------------------------------
            /*Hanya Polindrome*/
            /*Console.WriteLine("Soal 5\n");*/
            //Console.Write("Kata\t: ");
            //string kata = Console.ReadLine();
            //string temp = "";

            //for (int i = kata.Length - 1; i <= kata.Length && i >= 0; i--)
            //{
            //    temp += kata[i].ToString();
            //}
            //Console.WriteLine();

            //if (temp == kata)
            //{
            //    Console.WriteLine(kata + " is a Palindrome!");
            //}
            //else
            //{
            //    Console.WriteLine(kata + " is not a Palindrome!");
            //}

            Console.ReadKey();

        }
    }
}
