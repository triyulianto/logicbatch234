﻿using System;

namespace day3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Latihan Logic Day-3");
            Console.WriteLine("---------------------\n");

            Console.Write("Deret N: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine("\n");
            //Soal 1
            Console.WriteLine("Soal 1");
            int[,] deret = new int[2, n];
            int x = 0;
            int y = 1;
            Console.WriteLine();
            for (int i = 0; i <= 1; i++)
            {
                for (int j = 0; j < n; j++)
                {

                    if (i == 0)
                    {
                        deret[i, j] = x;
                        Console.Write(x + "\t");
                        x++;
                    }
                    if (i == 1)
                    {
                        deret[i, j] = y;
                        Console.Write(y + "\t");
                        y *= 3;
                    }

                }
                Console.WriteLine("\n");
            }
            Console.WriteLine("\n");

            //Soal 2
            Console.WriteLine("Soal 2");
            x = 0;
            y = 1;
            Console.WriteLine();
            for (int i = 0; i <= 1; i++)
            {
                for (int j = 0; j < n; j++)
                {

                    if (i == 0)
                    {
                        deret[i, j] = x;
                        Console.Write(x + "\t");
                        x++;
                    }
                    if (i == 1)
                    {
                        if ((j + 1) % 3 == 0)
                        {
                            deret[i, j] = y;
                            y *= (-1);
                            Console.Write(y + "\t");
                            y *= 3;
                            y *= (-1);
                        }
                        else
                        {
                            deret[i, j] = y;
                            Console.Write(y + "\t");
                            y *= 3;
                        }

                    }

                }
                Console.WriteLine("\n");
            }
            Console.WriteLine("\n");

            //Soal 3
            Console.WriteLine("soal 3");
            x = 0;
            y = 2;
            Console.WriteLine();
            for (int i = 0; i <= 1; i++)
            {
                for (int j = 0; j < 7; j++)
                {

                    if (i == 0)
                    {
                        deret[i, j] = x;
                        Console.Write(x + "\t");
                        x++;
                    }
                    if (i == 1)
                    {
                        deret[i, j] = y;
                        Console.Write(y + "\t");
                        if (j > 2)
                        {
                            y -= 4;
                        }
                        else
                        {
                            y += 4;
                        }
                    }

                }
                Console.WriteLine("\n");
            }
            Console.WriteLine("\n");

            //Soal 4
            Console.WriteLine("Soal 4");
            x = 0;
            y = 1;
            int z = 5;
            Console.WriteLine();
            for (int i = 0; i <= 1; i++)
            {
                for (int j = 0; j < n; j++)
                {

                    if (i == 0)
                    {
                        deret[i, j] = x;
                        Console.Write(x + "\t");
                        x++;
                    }
                    if (i == 1)
                    {
                        if ((j + 1) % 2 == 0)
                        {
                            deret[i, j] = z;
                            Console.Write(z + "\t");
                            z += 5;
                        }
                        else
                        {
                            deret[i, j] = y;
                            Console.Write(y + "\t");
                            y++;
                        }

                    }

                }
                Console.WriteLine("\n");
            }
            Console.WriteLine("\n");


            //Soal 5
            Console.WriteLine("Soal 5\n");
            Console.Write("Kalimat : ");
            string kalimat = Console.ReadLine();
            Console.WriteLine();
            string[] kata = kalimat.Split(" ");
            int jkata = kata.Length;
            string hasil = "";

            for(int i=0;i<jkata;i++)
            {
                hasil = kata[i];
                for(int j=0;j<hasil.Length;j++)
                {
                    if(j == (hasil.Length - hasil.Length))
                    {
                        Console.Write(hasil[j]);
                    }
                    else if(j == (hasil.Length - 1))
                    {
                        Console.Write(hasil[j] + " ");
                    }
                    else
                    {
                        Console.Write("*");
                    }
                    
                }
            }
            Console.WriteLine("\n");

            //Soal 6
            Console.WriteLine("Soal 6\n");
            Console.Write("Kalimat : ");
            kalimat = Console.ReadLine();
            int jumlah = 0;
            for (int i = 0; i < kalimat.Length; i++)
            {
                if (char.IsUpper(kalimat[i])) jumlah++;
            }
            Console.WriteLine("Jumlah Kata: "+jumlah);
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
