﻿using System;
using System.Collections.Generic;
using System.Text;

namespace day1
{
    public class Soal1
    {
        public static void jawabansoal1()
        {
            Console.Write("Banyak Deret: ");
            int n = int.Parse(Console.ReadLine());

            int deret = 1;

            for (int i = 1; i <= n; i++)
            {
                Console.Write(deret + " ");
                deret = deret + 2;

            }
            Console.ReadKey();
            Console.WriteLine();
        }
    }
}
