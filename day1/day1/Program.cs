﻿using System;

namespace day1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Latihan #day1");
            Console.WriteLine("-------------------");
            Console.WriteLine("Soal No.1");
            Console.WriteLine("Soal No.2");
            Console.WriteLine("Soal No.3");
            Console.WriteLine("Soal No.4");
            Console.WriteLine("Soal No.5");
            Console.WriteLine("Soal No.6");
            Console.WriteLine("Soal No.7");
            Console.WriteLine("Soal No.8");
            Console.WriteLine("Soal No.9");
            Console.WriteLine("Soal No.10");
            Console.WriteLine("Pilih Soal: ");
            int soal = int.Parse(Console.ReadLine());
            Console.WriteLine();
            switch (soal)
            {
                case 1:
                    Soal1.jawabansoal1();
                    break;
                case 2:
                    Soal2();
                    break;
                case 3:
                    Soal3();
                    break;
                case 4:
                    Soal4();
                    break;
                case 5:
                    Soal5();
                    break;
                case 6:
                    Soal6();
                    break;
                case 7:
                    Soal7();
                    break;
                case 8:
                    Soal8();
                    break;
                case 9:
                    Soal9();
                    break;
                case 10:
                    Soal10();
                    break;
                default:
                    break;
            }
            Console.WriteLine("\n");
            Console.ReadKey();
        }

        //Soal 2
        public static void Soal2()
        {
            Console.Write("Banyak Deret: ");
            int n = int.Parse(Console.ReadLine());
            int deret = 2;

            for (int i = 1; i <= n; i++)
            {
                Console.Write(deret + " ");
                deret = deret + 2;
            }
            Console.ReadKey();
            Console.WriteLine();
        }

        //Soal 3
        public static void Soal3()
        {
            Console.Write("Banyak Deret: ");
            int n = int.Parse(Console.ReadLine());
            int deret = 1;

            for (int i = 1; i <= n; i++)
            {
                Console.Write(deret + " ");
                deret = deret + 3;
            }
            Console.ReadKey();
            Console.WriteLine();
        }

        //Soal 4
        public static void Soal4()
        {
            Console.Write("Banyak Deret: ");
            int n = int.Parse(Console.ReadLine());
            int deret = 1;

            for (int i = 1; i <= n; i++)
            {
                Console.Write(deret + " ");
                deret = deret + 4;

            }
            Console.ReadKey();
        }

        //Soal 5
        public static void Soal5()
        {
            Console.Write("Banyak Deret: ");
            int n = int.Parse(Console.ReadLine());
            int deret = 1;
            int j = 1;
            for (int i = 1; i <= n; i++)
            {
                if (j % 3 == 0)
                {
                    Console.Write("* ");
                }
                else
                {
                    Console.Write(deret + " ");
                    deret = deret + 4;
                }
                j++;

            }
            Console.ReadKey();
        }

        //Soal 6
        public static void Soal6()
        {
            Console.Write("Banyak Deret: ");
            int n = int.Parse(Console.ReadLine());
            int deret = 1;
            int j = 1;
            for (int i = 1; i <= n; i++)
            {
                if (j % 3 == 0)
                {
                    Console.Write("* ");
                    deret = deret + 4;
                }
                else
                {
                    Console.Write(deret + " ");
                    deret = deret + 4;
                }
                j++;

            }
            Console.ReadKey();
        }

        //Soal 7
        public static void Soal7()
        {
            Console.Write("Banyak Deret: ");
            int n = int.Parse(Console.ReadLine());
            int deret = 2;
            int kali = 2;
            for (int i = 1; i <= n; i++)
            {
                Console.Write(deret + " ");
                deret = deret * kali;
            }
            Console.ReadKey();
        }

        //Soal 8
        public static void Soal8()
        {
            Console.Write("Banyak Deret: ");
            int n = int.Parse(Console.ReadLine());
            int deret = 3;
            int kali = 3;
            for (int i = 1; i <= n; i++)
            {
                Console.Write(deret + " ");
                deret = deret * kali;
            }
            Console.ReadKey();
        }

        //Soal 9
        public static void Soal9()
        {
            Console.Write("Banyak Deret: ");
            int n = int.Parse(Console.ReadLine());
            int deret = 4;
            int j = 1;
            for (int i = 1; i <= n; i++)
            {
                if (j % 3 == 0)
                {
                    Console.Write("* ");
                }
                else
                {
                    Console.Write(deret + " ");
                    deret = deret * 4;
                }
                j++;

            }
            Console.ReadKey();
        }

        //Soal 10
        public static void Soal10()
        {
            Console.Write("Banyak Deret: ");
            int n = int.Parse(Console.ReadLine());
            int deret = 3;
            int j = 1;
            for (int i = 1; i <= n; i++)
            {
                if (j % 4 == 0)
                {
                    Console.Write("xxx ");
                    deret = deret * 3;
                }
                else
                {
                    Console.Write(deret + " ");
                    deret = deret * 3;
                }
                j++;

            }
            Console.ReadKey();
        }

    }
}
