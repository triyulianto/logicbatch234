﻿using System;

namespace tugas1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input n deret: ");
            int n = int.Parse(Console.ReadLine());
            int j = 1;
            int a = 1;
            int b = 5;

            //Soal 1
            Console.WriteLine();
            Console.WriteLine("Soal 1");
            for (int i = 1; i <= n; i++)
            {
                if (j % 2 == 0)
                {
                    Console.Write(b + "\t");
                    b = b + 5;
                }
                else
                {
                    Console.Write(a + "\t");
                    a++;
                }
                j++;
            }

            Console.WriteLine();


            //Soal 2
            Console.WriteLine();
            Console.WriteLine("Soal 2");
            j = 1;
            a = 1;
            b = 5;
            for (int i = 1; i <= n; i++)
            {
                if (j % 3 == 0)
                {
                    Console.Write("*\t");
                }
                else if (j % 2 == 0)
                {
                    Console.Write(b + "\t");
                    b = b + 5;
                }
                else if (j % 5 == 0)
                {
                    Console.Write("@\t");
                }
                else
                {
                    Console.Write(a + "\t");
                    a++;
                }
                j++;
            }

            Console.WriteLine();

            //Soal 3 (Tugas)
            Console.WriteLine();
            Console.WriteLine("Soal 3");
            j = 1;
            a = 1;
            b = 5;
            for (int i = 1; i <= n; i++)
            {
                if (n % 2 == 0)
                {
                    int t1 = n / 2;
                    int t2 = n / 2 + 1;
                    if (j == 1)
                    {
                        Console.Write("*\t");
                        a++;
                    }
                    else if (j == n)
                    {
                        Console.Write("*\t");
                    }
                    else if (j == t1 && j % 2 == 0)
                    {
                        Console.Write("@\t");
                        b = b + 5;
                    }
                    else if (j == t1 && j % 2 != 0)
                    {
                        Console.Write("@\t");
                        a++;
                    }
                    else if (j == t2 && j % 2 == 0)
                    {
                        Console.Write("@\t");
                        b = b + 5;
                    }
                    else if (j == t2 && j % 2 != 0)
                    {
                        Console.Write("@\t");
                        a++;
                    }
                    else if (j % 2 == 0)
                    {
                        Console.Write(b + "\t");
                        b = b + 5;
                    }
                    else if (j % 2 != 0)
                    {
                        Console.Write(a + "\t");
                        a++;
                    }
                }
                else
                {
                    int t = (n + 1) / 2;

                    if (j == 1)
                    {
                        Console.Write("*\t");
                        a++;
                    }
                    else if (j == n)
                    {
                        Console.Write("*\t");
                    }
                    else if (j == t && j % 2 == 0)
                    {
                        Console.Write("@\t");
                        b = b + 5;
                    }
                    else if (j == t && j % 2 != 0)
                    {
                        Console.Write("@\t");
                        a++;
                    }
                    else if (j % 2 == 0)
                    {
                        Console.Write(b + "\t");
                        b = b + 5;
                    }
                    else if (j % 2 != 0)
                    {
                        Console.Write(a + "\t");
                        a++;
                    }
                }
                j++;
            }
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
