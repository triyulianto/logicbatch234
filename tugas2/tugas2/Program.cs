﻿using System;

namespace tugas2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tugas 2 - Logic");
            Console.WriteLine("--------------------------");
            Console.Write("Belanja\t: ");
            decimal belanja = decimal.Parse(Console.ReadLine());
            Console.Write("Jarak\t: ");
            decimal jarak = decimal.Parse(Console.ReadLine());
            decimal diskon = 0;
            decimal tbelanja = 0;
            if(belanja>30000)
            {
                diskon = belanja/100*40;
                if(diskon>30000)
                {
                    diskon = 30000;
                }
            }
            else
            {
                diskon = 0;
            }
            tbelanja = belanja - diskon;
            decimal tJarak = Math.Ceiling(jarak);      /*Pembulatan*/
            decimal ongkos = 0;
            if(tJarak>5)
            {
                ongkos = 5000 + ((tJarak - 5) * 1000);
            }
            else
            {
                ongkos = 5000;
            }
            decimal total = ongkos + tbelanja;
            Console.WriteLine();
            Console.WriteLine("Belanja\t\t: "+belanja);
            Console.WriteLine("Diskon (40%)\t: "+tbelanja);
            Console.WriteLine("Ongkir\t\t: "+ongkos);
            Console.WriteLine("Total Belanja\t: "+total);
            Console.WriteLine();
        }
    }
}
