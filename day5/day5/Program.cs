﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace day5
{
    class Program
    {
        static void Main(string[] args)
        {
        menu:
            Console.WriteLine("Latihan #day5");
            separator();
            Console.WriteLine("Soal No.1 - Sorting Nilai");
            Console.WriteLine("Soal No.2 - Jarak Tempuh");
            Console.WriteLine("Soal No.3 - Poin Pembelian Pulsa");
            Console.WriteLine("Soal No.4 - Recursive");
            Console.WriteLine("Soal No.5 - I am The Number One");
            Console.Write("Pilih Soal (Input No Soal)\t: ");
            int soal = int.Parse(Console.ReadLine());
            Console.WriteLine();
            switch (soal)
            {
                case 1:
                    Console.Clear();
                    soal1();
                    break;
                case 2:
                    Console.Clear();
                    soal2();
                    break;
                case 3:
                    Console.Clear();
                    soal3();
                    break;
                case 4:
                    Console.Clear();
                    soal4();
                    break;
                case 5:
                    Console.Clear();
                    soal5();
                    break;
                default:
                    break;
            }
            Console.WriteLine();
            Console.Write("Apakah anda ingin mengulang (y/n)\t: ");
            string ulang = Console.ReadLine();
            if (ulang.ToLower() == "y")
            {
                Console.Clear();
                goto menu;
            }
            else
            {
                Environment.Exit(0);
            }
            Console.ReadKey();
        }


        public static void soal1()
        {
            Console.WriteLine("Soal 1 - Sorting Nilai");
            separator();
            int lower, higher;
            Console.Write("Input deret nilai\t: ");
            string nilai = Console.ReadLine();
            int[] arr = nilai.Split(" ").Select(int.Parse).ToArray();
            for (int i = arr.Length - 1; i > 0; i--)
            {
                for (int j = 0; j < i; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
            Console.Write("Nilai Urut\t\t: ");
            for (int k = 0; k < arr.Length; k++)
            {
                Console.Write(arr[k] + " ");
            }
            lower = arr[0] + arr[1] + arr[2];
            higher = arr[arr.Length - 3] + arr[arr.Length - 2] + arr[arr.Length - 1];
            Console.WriteLine();
            Console.WriteLine("Jumlah 3 angka terkecil\t: " + lower);
            Console.WriteLine("Jumlah 3 angka terbesar\t: " + higher);
        }

        public static void soal2()
        {
            Console.WriteLine("Soal 5 - Jarak Tempuh");
            separator();
            Console.WriteLine("Keterangan Jarak Tempuh");
            Console.WriteLine("\t1) Jarak dari Toko ke Customer\t\t: 2 km");
            Console.WriteLine("\t2) Jarak dari Customer 1 ke Customer 2\t: 500 m");
            Console.WriteLine("\t3) Jarak dari Customer 2 ke Customer 3\t: 1.5 km");
            Console.WriteLine("\t4) Jarak dari Customer 3 ke Customer 4\t: 300 m");
            Console.WriteLine();
            int[] arah = new int[] { 2000, 500, 1500, 300 };
            Console.Write("Tujuan\t: ");
            string tujuan = Console.ReadLine();
            int[] jarak = tujuan.Split(' ').Select(int.Parse).ToArray();
            double total = 0;
            for (int i = 0; i < jarak.Length; i++)
            {
                total += arah[jarak[i] - 1];
            }
            Console.WriteLine();
            Console.WriteLine("Jarak yang ditempuh\t: " + total / 1000 + " m");
            Console.WriteLine("Bensin yang dibutuhkan\t: " + Math.Ceiling(total / 2500) + " liter");
        }

        public static void soal3()
        {
            Console.WriteLine("Soal No.3 - Poin Pembelian Pulsa");
            separator();
            Console.WriteLine("Bonus Poin Pembelian Pulsa;");
            Console.WriteLine("\t1) 0 - 10.000\t\t: 2 km");
            Console.WriteLine("\t2) 10.001 - 30.000\t: 500 m");
            Console.WriteLine("\t3) >30.000\t\t: 1.5 km");
            Console.WriteLine();
            Console.Write("Pembelian pulsa\t: ");
            int beli = int.Parse(Console.ReadLine());
            int[] poin = new int[3] { 0, 1, 2 };
            int tempPoin = 0;
            int totPoin = 0;
            for (int i = poin.Length - 1; i < poin.Length && i >= 0; i--)
            {
                if (beli > 30000)
                {
                    tempPoin = beli;
                    tempPoin -= 30000;
                    tempPoin = (tempPoin / 1000) * poin[i];
                    beli -= (beli - 30000);
                    totPoin += tempPoin;
                }
                else if (beli > 10000 && beli <= 30000)
                {
                    i = 1;
                    tempPoin = beli;
                    tempPoin -= 10000;
                    tempPoin = (tempPoin / 1000) * poin[i];
                    beli -= (beli - 10000);
                    totPoin += tempPoin;
                }
            }
            Console.WriteLine("Total bonus poin yang didapat " + totPoin);
        }

        public static void soal4()
        {
            Console.WriteLine("Soal No.4 - Recursive");
            separator();
            Console.Write("Input Recursive\t: ");
            string aNilai = Console.ReadLine();
            int[] aSplit = aNilai.Split(' ').Select(int.Parse).ToArray();
            char[] pecahan = aSplit[0].ToString().ToCharArray();
            int temp = 0;
            int data = 0;

        rekursif:
            for (int j = 0; j < pecahan.Length; j++)
            {
                data = int.Parse(pecahan[j].ToString());
                temp += data;
            }
            temp *= aSplit[1];

            if (temp > 9)
            {

                pecahan = temp.ToString().ToCharArray();
                temp = 0;
                aSplit[1] = 1;
                goto rekursif;
            }
            Console.WriteLine("Hasil\t: " + temp);

        }

        public static void soal5()
        {
            Console.WriteLine("Soal 5 - I am 'The Number One'");
            Console.Write("Masukkan jumlah angka yang diinginkan : ");
            int input = int.Parse(Console.ReadLine());
            int count = 0;
            for (int i = 100; i < 1000; i++)
            {
                string angka = i.ToString();
            mulai:
                int[] square = new int[angka.Length];
                for (int j = 0; j < square.Length; j++)
                {
                    string test = angka[j].ToString();
                    square[j] = int.Parse(test) * int.Parse(test);
                }
                int sum = 0;
                for (int k = 0; k < square.Length; k++)
                {
                    sum += square[k];
                }
                if (sum == 1)
                {
                    Console.WriteLine(i + " is the ONE number");
                    count++;
                }
                else if (sum > 9)
                {
                    angka = sum.ToString();
                    goto mulai;
                }
                if (count == input)
                {
                    break;
                }

            }


        }

        public static void separator()
        {
            Console.WriteLine("- - - - - - - - - - - - - - -");
        }
    }
}
