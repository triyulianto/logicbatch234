﻿using System;

namespace day2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input n deret: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine("------------------------------");
            Console.WriteLine("\n");


            //soal 1
            Console.WriteLine("Soal 1 \t#Fibonacci 2");
            Console.WriteLine();
            int a = 0;
            int b = 1;
            int f = 1;
            for (int i = 1; i <= n; i++)
            {
                Console.Write(f + "\t");
                f = a + b;
                a = b;
                b = f;
            }
            Console.WriteLine("\n\n\n");


            //soal 2
            Console.WriteLine("Soal 2 \t#Fibonacci 3");
            Console.WriteLine();
            a = 1;
            b = 1;
            int c = 1;
            f = 1;
            Console.Write(a + "\t" + b + "\t" + c + "\t");
            for (int i = 3; i < n; i++)
            {
                f = a + b + c;
                a = b;
                b = c;
                c = f;
                Console.Write(f + "\t");
            }

            Console.WriteLine("\n\n\n");

            ////soal 3
            Console.WriteLine("Soal 3 \t#Bilangan Prima");
            Console.WriteLine();
            a = 0;
            b = 0;
            for (int i = 2; i <= n; i++)
            {
                a = 2;
                for (a = a; a <= i - 1;)
                {
                    if (i % a == 0)
                    {
                        b = b + 1;
                    }
                    a++;
                }

                if (b == 0)
                {
                    Console.Write(i + "\t");
                }
                b = 0;
            }
            Console.WriteLine("\n\n\n");

            //soal 4
            Console.WriteLine("Pohon Faktorial");
            Console.Write("Input Nilai : ");
            int angka = int.Parse(Console.ReadLine());
            int hasil = angka;
            int j = 0;
            for (int i = 0; i <= angka; i++)
            {
                for (j = 2; j <= i; j++)
                {
                    if (angka % j == 0)
                    {
                        Console.Write(j + "\t");
                        angka /= j;
                    }
                    if (j == angka)
                    {
                        Console.Write(j + "\t");
                        angka /= j;
                    }
                }
            }

            if (angka == 1 || angka == hasil)
            {
                Console.Write("1");
            }
            Console.WriteLine("\n\n\n");

            //Soal 5
            Console.WriteLine("Pohon Faktorial");
            Console.Write("Input Nilai : ");
            angka = int.Parse(Console.ReadLine());
            hasil = angka;
            int pembagi = 0;
            j = 0;
            for (int i = 0; i <= angka; i++)
            {
                for (j = 2; j <= i; j++)
                {
                    if (angka % j == 0)
                    {
                        Console.Write(j + "\t");
                        pembagi += j;
                        angka /= j;
                    }
                    if (j == angka)
                    {
                        Console.Write(j + "\t");
                        pembagi += j;
                        angka /= j;
                    }
                }
            }

            if (angka == 1 || angka == hasil)
            {
                Console.Write("1");
                pembagi += 1;
            }
            Console.WriteLine();
            Console.WriteLine("Jumlah nilai pembagi \t: " + pembagi);
            Console.WriteLine("\n\n\n");

            Console.ReadKey();

        }
    }
}
